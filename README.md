MOOC Platform Challenge
================

## The Challenge

The challenge, if you choose to accept it, is to create a platform to help the world learn online.

## Requirements

1. Create a backend API with the following three endpoints (Slide down to check the API documentation) - Django is preferred but feel free to use what you are familiar with:
    * **POST** /course
    * **GET** /courses
    * **GET** /:course_id
    * **PUT** /:course_id

2. Modify courses list API endpoint to optionally sort the courses based on ```starts_at``` field ascendingly.

3. Create a frontend website with the following pages (ReactJS is highly encouraged):
    * Landing page (we want you to be creative in this page)
    * List courses
    * View course
    * Add new course
    * Edit existing course

4. Write API unit tests.

**SUGGESTED BONUS Requirements** the following are bonus requirements that can get you more noticed, but feel free to add more to the app as you please.

1. Add new ```category``` model (each course will belong to a category).

1. Add three new API endpoints:
    * **GET** /categories
    * **GET** /:category_id
    * **POST** /category

1. Add the following pages to the frontend website:
    * Categories page (list all categories).
    * Category page (shows category name and the courses that are belonging to it).
    * Add new category page.

1. Implement a flux based application architecture on your FrontEnd

1. Use OAuth2 in the project.

1. Use youtube API to add videos to the courses you are building

1. Use a continuous integration (CI) tool.

1. Deploy the app

## Submission
* Fork this project and send us a link to your fork


## Tips

* It must be well tested, it must also be possible to run the entire test suit with a single command from the directory of your repository.
* Less is more, small is beautiful, you know the drill — stick to the requirements.
* Use the right tools, Django and ReactJS is highly encouraged.
* Unit tests, but be careful with untested parts of the system.
* Good information architecture and Database design can take you a long way
* High test coverage, and the use of E2E test tools is a big Plus
* The code must be well organized and styled (readable and maintainable)
* The codebase should be scalable and performant
* Good documentation can go a long way
* Good git branching strategy will make a huge difference

**Good Luck!** — not that you need any ;)

-------------------------------------------------------------------------

## API Documentation

**All responses must be encoded in JSON and have the appropriate Content-Type header**


### POST /course

```
POST /course
Content-Type: "application/json"

{
  ...
}
```

Attribute       | Description
----------------| -----------
**name**        | course name
**square_image**| square course image url
**starts_at**   | course starting date and time (unix timestamp format)

##### Returns:

```
200 Success
Content-Type: "application/json"

{
  ...
}
```

The course is created.

##### Errors:

Error | Description
----- | ------------
400   | ```name```, ```square_image``` or ```starts_at``` is not present
400   | The course ```name``` is exist
400   | ```starts_at``` must be in the future


### GET /courses

```
GET /courses
Content-Type: "application/json"
```

##### Returns:

```
200 Success
Content-Type: "application/json"

[
  ...
  ...
]
```


### GET /:course_id

```
GET /:course_id
Content-Type: "application/json"
```

Attribute      | Description
-------------- | -----------
**course_id**  | course identifier

##### Returns

```
200 Success
Content-Type: "application/json"

{
  ...
}
```

##### Errors

Error | Description
----- | ------------
404   | The ```course_id``` cannot be found in the system



## Assessment Criteria
These are what we will look for in the solution:

* **Readability:** Coding style, method/variable/class/etc.
* **Structure:** Object design and code architecture. Try not to overarchitect your solution.
* **Documentation:** Clarity of communication in your documentation (code comments, documents if needed). Also, the quality of your commit messages.
* **Testing:** Approach to testing.
* **Creativity:** in choosing how to present the landing page
